# Componentes

Projeto contendo os arquivos fontes com as especificações dos Componentes do DS.

## Contribuindo

Antes de abrir um Merge Request tenha em mente algumas informações:

- Esse é um projeto opensource e contribuições são bem-vindas.
- Para facilitar a aprovação da sua contribuição, escolha um título curto, simples e explicativo para o MR, e siga os padrões da nossa [wiki](https://gov.br/ds/wiki/ 'Wiki').
- Quer contribuir com o projeto? Confira o nosso guia [como contribuir](./CONTRIBUTING.md 'Como contribuir?').

### Reportar bugs/necessidades

Você pode usar as [issues](https://gitlab.com/govbr-ds/ds/design/artefatos/govbr-ds-design-components-src/-/issues/new) para nos informar os problemas que tem enfrentado ao usar nossa biblioteca ou mesmo o que gostaria que fizesse parte do projeto. Por favor use o modelo que mais se encaixa na sua necessidade e preencha com o máximo de detalhes possível.

Nos comprometemos a responder a todas as issues.

## Precisa de ajuda?

> Por favor **não** crie issues para fazer perguntas...

Use nossos canais abaixo para obter tirar suas dúvidas:

- Site do GovBR-DS <http://gov.br/ds>

- Pelo nosso email <govbr-ds@serpro.gov.br>

- Usando nosso canal no discord <https://discord.gg/U5GwPfqhUP>

## Commits

Nesse projeto usamos um padrão para branches e commits. Por favor observe a documentação na nossa [wiki](https://gov.br/ds/wiki/ 'Wiki') para aprender sobre os nossos padrões.

## Créditos

Os Web Components do [GovBR-DS](https://gov.br/ds/ 'GovBR-DS') são criados pelo [SERPRO](https://www.serpro.gov.br/ 'SERPRO | Serviço Federal de Processamento de Dados') juntamente com a participação da comunidade.

## Licença

Nesse projeto usamos a licença CC0 1.0.
